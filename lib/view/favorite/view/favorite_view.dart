import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../core/base/view/base_view.dart';
import '../../../core/component/animation/fade_in_gridview_animation.dart';
import '../../../core/component/loading/loading_form.dart';
import '../../../core/extension/context_extension.dart';
import '../../../core/extension/string_extension.dart';
import '../../../core/init/language/locale_keys.g.dart';
import '../../../product/widget/alert-dialog/button_alert_dialog.dart';
import '../../../product/widget/card/movie_card.dart';
import '../model/search_by_id_response_model.dart';
import '../view-model/favorite_view_model.dart';

class FavoriteView extends StatelessWidget {
  const FavoriteView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseView<FavoriteViewModel>(
      viewModel: FavoriteViewModel(),
      onModelReady: (model) {
        model.setContext(context);
        model.init();
      },
      onDispose: () {},
      onPageBuilder: (BuildContext context, FavoriteViewModel state) => _scaffold(context, state),
    );
  }

  Widget _scaffold(BuildContext context, FavoriteViewModel state) {
    return Observer(builder: (_) {
      return Scaffold(
        appBar: _scaffoldAppBar(),
        body: state.isLoading ? LoadingForm() : _scaffoldBody(context, state),
      );
    });
  }

  AppBar _scaffoldAppBar() {
    return AppBar(
      title: Text(LocaleKeys.favorite_appBarName.locale),
      centerTitle: true,
    );
  }

  Widget _scaffoldBody(BuildContext context, FavoriteViewModel state) {
    return RefreshIndicator(
      onRefresh: () async => state.fetchSearchById(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          state.isThereAnyItem() ? _movieGridList(state) : _favoriteListEmpty(context),
        ],
      ),
    );
  }

  Widget _favoriteListEmpty(BuildContext context) {
    return Center(child: Text(LocaleKeys.favorite_notFoundMessage.locale, style: context.textTheme.bodyText1));
  }

  Expanded _movieGridList(FavoriteViewModel state) {
    var sliverGridDelegateWithFixedCrossAxisCount =
        SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: .6, mainAxisSpacing: 0, crossAxisSpacing: 1);

    return Expanded(
      child: QFadeInAnimationGridViewConfig(
        index: 1,
        columnCount: 2,
        child: GridView.builder(
          shrinkWrap: true,
          gridDelegate: sliverGridDelegateWithFixedCrossAxisCount,
          itemBuilder: (context, index) {
            var _item = state.searchByIdListResult![index];
            var _isFavoriteItem = state.isFavoriteItem(_item.imdbId!);

            return _gridViewItemCard(_item, _isFavoriteItem, context, state);
          },
          itemCount: state.searchByIdListResult!.length,
        ),
      ),
    );
  }

  MovieCard _gridViewItemCard(SearchByIdResponseModel _item, bool _isFavoriteItem, BuildContext context, FavoriteViewModel state) {
    return MovieCard(
      poster: _item.poster!,
      title: _item.title!,
      onPressed: () async => _isFavoriteItem
          ? await showWarningDialog(context, state, _item.imdbId!)
          : await state.splashViewModel.localeManagerFavoriteListAdd(_item.imdbId!),
      isFavoriteItem: _isFavoriteItem,
    );
  }

  Future<void> showWarningDialog(BuildContext context, FavoriteViewModel state, String imdbId) async {
    await showDialog(
        context: context,
        builder: (context) {
          return ButtonAlertDialog(
            header: LocaleKeys.warningHeader.locale,
            message: LocaleKeys.warningMessage.locale,
            imdbId: imdbId,
            state: state,
          );
        });
  }
}
