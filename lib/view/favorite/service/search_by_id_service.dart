import 'package:vexana/src/interface/INetworkService.dart';
import 'package:vexana/vexana.dart';

import '../model/search_by_id_response_model.dart';
import 'ISearchByIdService.dart';

class SearchByIdService extends ISearchByIdService {
  SearchByIdService(INetworkManager manager) : super(manager);

  @override
  Future<SearchByIdResponseModel?> fetchSearchById(Map<String, dynamic> params) async {
    final response = await manager.send<SearchByIdResponseModel, SearchByIdResponseModel>(
      '',
      parseModel: SearchByIdResponseModel(),
      method: RequestType.GET,
      queryParameters: params,
    );

    if (response.data is SearchByIdResponseModel) {
      return response.data;
    } else {
      return null;
    }
  }
}
