import 'package:vexana/vexana.dart';

import '../model/search_by_id_response_model.dart';

abstract class ISearchByIdService {
  final INetworkManager manager;

  ISearchByIdService(this.manager);

  Future<SearchByIdResponseModel?> fetchSearchById(Map<String, dynamic> params);
}
