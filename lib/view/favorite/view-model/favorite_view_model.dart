import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import '../../../core/base/model/base_view_model.dart';
import '../../../core/constant/app/api_constants.dart';
import '../../../core/extension/string_extension.dart';
import '../../../core/init/language/locale_keys.g.dart';
import '../../../core/init/network/vexana_manager.dart';
import '../../../product/constant/splash_constant.dart';
import '../../splash/view-model/splash_view_model.dart';
import '../model/search_by_id_response_model.dart';
import '../service/ISearchByIdService.dart';
import '../service/search_by_id_service.dart';

part 'favorite_view_model.g.dart';

class FavoriteViewModel = _FavoriteViewModelBase with _$FavoriteViewModel;

abstract class _FavoriteViewModelBase with Store, BaseViewModel {
  late ISearchByIdService searchByIdService;
  late SplashViewModel splashViewModel;

  @override
  void setContext(BuildContext context) {
    this.context = context;
  }

  @override
  Future<void> init({dynamic moveData}) async {
    searchByIdService = SearchByIdService(VexanaManager.instance.movieManager);
    searchByIdListResult = <SearchByIdResponseModel>[];
    splashViewModel = SplashViewModel();

    await fetchSearchById();
  }

  //*kontrol değişkenleri
  @observable
  bool isLoading = false;
  @action
  void isLoadingChange() => isLoading = !isLoading;

  @observable
  List<SearchByIdResponseModel>? searchByIdListResult;

  //* İstek sonrası eleman varmı kontrolü
  @action
  bool isThereAnyItem() {
    if (isLoading) {
      return false;
    } else if (searchByIdListResult!.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  //* Açılışta Splash ekranında cihaz hafızasından okunan film listesi , film id ile servisten çağırmak için kullanılır
  @action
  Future<void> fetchSearchById() async {
    isLoadingChange();

    if (await connectivity.connectivityStatus()) {
      searchByIdListResult = [];
      if (SplashConstant.instance.favoriteList != null) {
        for (var imdbID in SplashConstant.instance.favoriteList!) {
          var params = {ApiConstants.P_I: imdbID, ApiConstants.P_PLOT: 'short'};
          var response = await searchByIdService.fetchSearchById(params);
          searchByIdListResult!.add(response!);
        }
      }
      isLoadingChange();
    } else {
      isLoadingChange();
      await showErrorMessage(context!, LocaleKeys.errorHeader.locale, LocaleKeys.noConnection.locale);
    }
  }

  // Future<void> gotoMovieDetailView() async {
  //   await navigation.navigateToPageClear(path: NavigationConstants.MOVIE_DETAIL_VIEW);
  // }

  //* Filmin favori listesinde olup olmadığı kontrol edilir
  bool isFavoriteItem(String imdbID) {
    var isFavorite = SplashConstant.instance.favoriteList!.contains(imdbID);
    return isFavorite;
  }

  //* Favoriye eklenen veya favoriden kaldırılan  filmleri telefon hafızasında günceller
  //* vvv

  Future<void> favoriteListAdd(String imdbID) async {
    await splashViewModel.localeManagerFavoriteListAdd(imdbID);
    showToast(LocaleKeys.addFavoriteList.locale);
  }

  Future<void> favoriteListRemove(String imdbID) async {
    await splashViewModel.localeManagerFavoriteListRemove(imdbID);
    showToast(LocaleKeys.removeFavoriteList.locale);
  }
}
