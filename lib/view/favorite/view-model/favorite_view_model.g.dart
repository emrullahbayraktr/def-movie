// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$FavoriteViewModel on _FavoriteViewModelBase, Store {
  final _$isLoadingAtom = Atom(name: '_FavoriteViewModelBase.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$searchByIdListResultAtom =
      Atom(name: '_FavoriteViewModelBase.searchByIdListResult');

  @override
  List<SearchByIdResponseModel>? get searchByIdListResult {
    _$searchByIdListResultAtom.reportRead();
    return super.searchByIdListResult;
  }

  @override
  set searchByIdListResult(List<SearchByIdResponseModel>? value) {
    _$searchByIdListResultAtom.reportWrite(value, super.searchByIdListResult,
        () {
      super.searchByIdListResult = value;
    });
  }

  final _$fetchSearchByIdAsyncAction =
      AsyncAction('_FavoriteViewModelBase.fetchSearchById');

  @override
  Future<void> fetchSearchById() {
    return _$fetchSearchByIdAsyncAction.run(() => super.fetchSearchById());
  }

  final _$_FavoriteViewModelBaseActionController =
      ActionController(name: '_FavoriteViewModelBase');

  @override
  void isLoadingChange() {
    final _$actionInfo = _$_FavoriteViewModelBaseActionController.startAction(
        name: '_FavoriteViewModelBase.isLoadingChange');
    try {
      return super.isLoadingChange();
    } finally {
      _$_FavoriteViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool isThereAnyItem() {
    final _$actionInfo = _$_FavoriteViewModelBaseActionController.startAction(
        name: '_FavoriteViewModelBase.isThereAnyItem');
    try {
      return super.isThereAnyItem();
    } finally {
      _$_FavoriteViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isLoading: ${isLoading},
searchByIdListResult: ${searchByIdListResult}
    ''';
  }
}
