import 'package:json_annotation/json_annotation.dart';
import 'package:vexana/vexana.dart';

part 'search_by_id_response_model.g.dart';

@JsonSerializable()
class SearchByIdResponseModel extends INetworkModel<SearchByIdResponseModel> {
  SearchByIdResponseModel({
    this.title,
    this.year,
    this.rated,
    this.released,
    this.runtime,
    this.genre,
    this.director,
    this.writer,
    this.actors,
    this.plot,
    this.language,
    this.country,
    this.awards,
    this.poster,
    this.ratings,
    this.metascore,
    this.imdbRating,
    this.imdbVotes,
    this.imdbId,
    this.type,
    this.dvd,
    this.boxOffice,
    this.production,
    this.website,
    this.response,
  });

  @JsonKey(name: 'Title')
  String? title;
  @JsonKey(name: 'Year')
  String? year;
  @JsonKey(name: 'Rated')
  String? rated;
  @JsonKey(name: 'Released')
  String? released;
  @JsonKey(name: 'Runtime')
  String? runtime;
  @JsonKey(name: 'Genre')
  String? genre;
  @JsonKey(name: 'Director')
  String? director;
  @JsonKey(name: 'Writer')
  String? writer;
  @JsonKey(name: 'Actors')
  String? actors;
  @JsonKey(name: 'Plot')
  String? plot;
  @JsonKey(name: 'Language')
  String? language;
  @JsonKey(name: 'Country')
  String? country;
  @JsonKey(name: 'Awards')
  String? awards;
  @JsonKey(name: 'Poster')
  String? poster;
  @JsonKey(name: 'Ratings')
  List<Rating>? ratings;
  @JsonKey(name: 'Metascore')
  String? metascore;
  @JsonKey(name: 'imdbRating')
  String? imdbRating;
  @JsonKey(name: 'imdbVotes')
  String? imdbVotes;
  @JsonKey(name: 'imdbID')
  String? imdbId;
  @JsonKey(name: 'Type')
  String? type;
  @JsonKey(name: 'DVD')
  String? dvd;
  @JsonKey(name: 'BoxOffice')
  String? boxOffice;
  @JsonKey(name: 'Production')
  String? production;
  @JsonKey(name: 'Website')
  String? website;
  @JsonKey(name: 'Response')
  String? response;

  @override
  SearchByIdResponseModel fromJson(Map<String, dynamic> json) {
    return _$SearchByIdResponseModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$SearchByIdResponseModelToJson(this);
  }
}

@JsonSerializable()
class Rating extends INetworkModel<Rating> {
  Rating({
    this.source,
    this.value,
  });

  @JsonKey(name: 'Source')
  String? source;
  @JsonKey(name: 'Value')
  String? value;

  @override
  Rating fromJson(Map<String, dynamic> json) {
    return _$RatingFromJson(json);
  }

  factory Rating.fromJson(Map<String, dynamic> json) {
    return _$RatingFromJson(json);
  }
  @override
  Map<String, dynamic> toJson() {
    return _$RatingToJson(this);
  }
}
