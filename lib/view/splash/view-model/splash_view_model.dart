import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

import '../../../core/base/model/base_view_model.dart';
import '../../../core/constant/enum/app_theme_enum.dart';
import '../../../core/constant/enum/locale_keys_enum.dart';
import '../../../core/constant/navigation/navigation_constants.dart';
import '../../../core/extension/string_extension.dart';
import '../../../core/init/language/locale_keys.g.dart';
import '../../../core/init/notifier/theme_notifier.dart';
import '../../../product/constant/splash_constant.dart';

part 'splash_view_model.g.dart';

class SplashViewModel = _SplashViewModelBase with _$SplashViewModel;

abstract class _SplashViewModelBase with Store, BaseViewModel {
  @override
  void setContext(BuildContext context) => this.context = context;
  @override
  void init({dynamic moveData}) {}
  @observable
  bool isLoading = false;
  @action
  void isLoadingChange() => isLoading = !isLoading;

  Future<void> splashControl() async {
    isLoadingChange();
    //* mainde okunan tema renk bilgisi provider aracılığıyla  uygulama teması setleniyor
    Provider.of<ThemeNotifier>(context!, listen: false).changeValue(SplashConstant.instance.isDarkTheme ?? false ? AppThemes.DARK : AppThemes.LIGHT);
    //* locale managerdan favori listesi okunuyor
    SplashConstant.instance.favoriteList = localeManager.getStringList(PreferenceKeys.FAVORITE_LIST);

    //* ekran 2 saniye süreyle bekletiliyor
    await Future.delayed(Duration(milliseconds: 2000));

    //* connectivity sınıfı ile internet bağlantısı kontrol ediliyor
    if (await connectivity.connectivityStatus()) {
      await gotoSearchView();
      isLoadingChange();
    } else {
      isLoadingChange();
      await showErrorMessage(context!, LocaleKeys.errorHeader.locale, LocaleKeys.noConnection.locale);
    }
  }

  Future<void> gotoSearchView() async {
    await navigation.navigateToPageClear(path: NavigationConstants.SEARCH_VIEW);
  }

  //* Favoriye eklenen veya favoriden kaldırılan  filmleri telefon hafızasında günceller
  //* vvv

  void localeManagerGetFavoriteList() => SplashConstant.instance.favoriteList = localeManager.getStringList(PreferenceKeys.FAVORITE_LIST);

  Future<void> localeManagerFavoriteListAdd(String imdbID) async {
    SplashConstant.instance.favoriteList ??= [];
    SplashConstant.instance.favoriteList!.add(imdbID);
    await localeManager.setStringList(PreferenceKeys.FAVORITE_LIST, SplashConstant.instance.favoriteList!);
    localeManagerGetFavoriteList();
  }

  Future<void> localeManagerFavoriteListRemove(String imdbID) async {
    SplashConstant.instance.favoriteList ??= [];
    SplashConstant.instance.favoriteList!.remove(imdbID);
    await localeManager.setStringList(PreferenceKeys.FAVORITE_LIST, SplashConstant.instance.favoriteList!);
    localeManagerGetFavoriteList();
  }
}
