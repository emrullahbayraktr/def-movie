import 'package:flutter/material.dart';

import '../../../core/base/view/base_view.dart';
import '../../../core/component/loading/loading_form.dart';
import '../view-model/splash_view_model.dart';

class SplashView extends StatelessWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseView<SplashViewModel>(
      viewModel: SplashViewModel(),
      onModelReady: (model) {
        model.setContext(context);
        model.init();
        Future.microtask(() => model.splashControl());
      },
      onDispose: () {},
      onPageBuilder: (BuildContext context, SplashViewModel state) => _scaffold(context),
    );
  }

  Scaffold _scaffold(BuildContext context) {
    return Scaffold(
      body: LoadingForm(),
    );
  }
}
