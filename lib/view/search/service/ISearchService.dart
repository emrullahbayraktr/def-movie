import 'package:vexana/vexana.dart';

import '../model/search_response_model.dart';

abstract class ISearchService {
  final INetworkManager manager;

  ISearchService(this.manager);

  Future<SearchResponseModel?> fetchMovie(Map<String, dynamic> params);
}
