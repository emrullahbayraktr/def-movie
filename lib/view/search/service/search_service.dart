import 'package:vexana/vexana.dart';

import '../model/search_response_model.dart';
import 'ISearchService.dart';

class SearchService extends ISearchService {
  SearchService(INetworkManager manager) : super(manager);

  @override
  Future<SearchResponseModel?> fetchMovie(Map<String, dynamic> params) async {
    final response = await manager.send<SearchResponseModel, SearchResponseModel>(
      '',
      parseModel: SearchResponseModel(),
      method: RequestType.GET,
      queryParameters: params,
    );

    if (response.data is SearchResponseModel) {
      return response.data;
    } else {
      return null;
    }
  }
}
