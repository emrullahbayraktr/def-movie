import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../core/base/view/base_view.dart';
import '../../../core/component/animation/fade_in_gridview_animation.dart';
import '../../../core/component/loading/loading_form.dart';
import '../../../core/component/text-field/no_border_text_form_field.dart';
import '../../../core/extension/context_extension.dart';
import '../../../core/extension/string_extension.dart';
import '../../../core/extension/widget_extension.dart';
import '../../../core/init/language/locale_keys.g.dart';
import '../../../product/constant/image_path.dart';
import '../../../product/widget/card/movie_card.dart';
import '../model/search_response_model.dart';
import '../view-model/search_view_model.dart';

class SearchView extends StatefulWidget {
  const SearchView({Key? key}) : super(key: key);

  @override
  _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<SearchViewModel>(
      viewModel: SearchViewModel(),
      onModelReady: (model) {
        model.setContext(context);
        model.init();
      },
      onDispose: () {},
      onPageBuilder: (BuildContext context, SearchViewModel state) => _scaffold(context, state),
    );
  }

  void buildScrollListener(SearchViewModel state) {
    Future<void> _scrollListener() async {
      if (state.scrollController!.position.atEdge) {
        if (state.scrollController!.position.pixels > 0) {
          await state.moreFetchMovie();
        }
      }
    }

    state.scrollController!.addListener(_scrollListener);
  }

  Widget _scaffold(BuildContext context, SearchViewModel state) {
    return Observer(builder: (_) {
      buildScrollListener(state);

      return Scaffold(
        appBar: _scaffoldAppBar(context, state),
        floatingActionButton: _scaffoldFABButton(context, state),
        body: state.isLoading ? LoadingForm() : _scaffoldBody(state, context),
      );
    });
  }

  AppBar _scaffoldAppBar(BuildContext context, SearchViewModel state) {
    return AppBar(
      title: Text(LocaleKeys.appName.locale),
      centerTitle: true,
      leading: _appBarLeadingFavorite(context, state),
      actions: [_appBarActionSwitch(state, context)],
    );
  }

  Padding _appBarLeadingFavorite(BuildContext context, SearchViewModel state) {
    return Padding(
        padding: context.paddingNormalHorizontal,
        child: IconButton(onPressed: () => state.gotoFavoriteView(), icon: Icon(Icons.favorite, color: context.colors.error.withOpacity(0.8))));
  }

  Switch _appBarActionSwitch(SearchViewModel state, BuildContext context) {
    return Switch(
        activeColor: Colors.transparent,
        activeThumbImage: AssetImage(ImagePath.instance.dark),
        inactiveThumbColor: Colors.transparent,
        inactiveThumbImage: AssetImage(ImagePath.instance.light),
        value: state.isDark,
        onChanged: (value) => state.onThemeChange(context));
  }

  Widget _scaffoldFABButton(BuildContext context, SearchViewModel state) {
    return state.searchList!.isNotEmpty
        ? FloatingActionButton(
            heroTag: null,
            backgroundColor: context.colors.primary,
            mini: true,
            onPressed: () {
              state.scrollController!.animateTo(1, duration: Duration(seconds: 1), curve: Curves.fastOutSlowIn);
            },
            child: Icon(Icons.keyboard_arrow_up, color: context.colors.onBackground))
        : Container();
  }

  Widget _scaffoldBody(SearchViewModel state, BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async => await state.refreshList(),
      child: Form(
        key: state.formState,
        child: Column(
          children: [
            _searchText(context, state),
            _bodyGridView(state, context),
          ],
        ),
      ),
    );
  }

  Expanded _bodyGridView(SearchViewModel state, BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          state.isThereAnyItem() ? _movieGridList(state) : _movieEmpty(context),
        ],
      ),
    );
  }

  Widget _movieEmpty(BuildContext context) {
    return Center(
      child: Text(LocaleKeys.search_infoMessage.locale, style: context.textTheme.bodyText1),
    );
  }

  Widget _movieGridList(SearchViewModel state) {
    var sliverGridDelegateWithFixedCrossAxisCount =
        SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: .6, mainAxisSpacing: 0, crossAxisSpacing: 1);

    return Expanded(
      child: Column(
        children: [
          Expanded(
            child: QFadeInAnimationGridViewConfig(
              index: 1,
              columnCount: 2,
              child: GridView.builder(
                controller: state.scrollController,
                shrinkWrap: true,
                gridDelegate: sliverGridDelegateWithFixedCrossAxisCount,
                itemBuilder: (context, index) {
                  var _item = state.searchList![index];
                  var _isFavoriteItem = state.isFavoriteItem(_item.imdbId!);

                  return _gridViewItemCard(_item, _isFavoriteItem, state);
                },
                itemCount: state.searchList!.length,
              ),
            ),
          ),
          CircularProgressIndicator().toVisible(state.isLoading)
        ],
      ),
    );
  }

  MovieCard _gridViewItemCard(Search _item, bool _isFavoriteItem, SearchViewModel state) {
    return MovieCard(
      poster: _item.poster!,
      title: _item.title!,
      onPressed: () async {
        _isFavoriteItem
            ? await state.splashViewModel.localeManagerFavoriteListRemove(_item.imdbId!)
            : await state.splashViewModel.localeManagerFavoriteListAdd(_item.imdbId!);
        // _isFavoriteItem ? await state.localeManagerFavoriteListRemove(_item.imdbId!) : await state.localeManagerFavoriteListAdd(_item.imdbId!);
        setState(() {});
      },
      isFavoriteItem: _isFavoriteItem,
    );
  }

  Padding _searchText(BuildContext context, SearchViewModel state) {
    return Padding(
      padding: context.paddingNormal,
      child: Card(
        elevation: 10,
        child: QNoBorderTextFormField(
          controller: state.searchController!,
          suffixIcon: buildSearchButton(context, state),
          onFieldSubmitted: () {},
          onValidator: (val) => LocaleKeys.search_searchValidator.locale,
          onSaved: (value) => state.searchController!.text = value,
          hintText: LocaleKeys.search_searchLabel.locale,
          labelText: '',
          textStyle: context.textTheme.headline6!.copyWith(fontWeight: FontWeight.w300),
          textInputType: TextInputType.name,
        ),
      ),
    );
  }

  Widget buildSearchButton(BuildContext context, SearchViewModel state) {
    return TextButton.icon(
      style: TextButton.styleFrom(alignment: Alignment.centerRight),
      onPressed: () async {
        state.searchList!.clear();
        await state.fetchMovie(1);
      },
      icon: Icon(Icons.search),
      label: SizedBox.shrink(),
    );
  }
}
