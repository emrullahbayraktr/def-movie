import 'package:json_annotation/json_annotation.dart';
import 'package:vexana/vexana.dart';

part 'search_response_model.g.dart';

@JsonSerializable()
class SearchResponseModel extends INetworkModel<SearchResponseModel> {
  SearchResponseModel({
    this.search,
    this.totalResults,
    this.response,
  });
  @JsonKey(name: 'Search')
  List<Search>? search;
  String? totalResults;
  @JsonKey(name: 'Response')
  String? response;

  @override
  SearchResponseModel fromJson(Map<String, dynamic> json) {
    return _$SearchResponseModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$SearchResponseModelToJson(this);
  }
}

@JsonSerializable()
class Search extends INetworkModel<Search> {
  Search({
    this.title,
    this.year,
    this.imdbId,
    this.type,
    this.poster,
  });

  @JsonKey(name: 'Title')
  String? title;
  @JsonKey(name: 'Year')
  String? year;
  @JsonKey(name: 'imdbID')
  String? imdbId;
  @JsonKey(name: 'Type')
  String? type;
  @JsonKey(name: 'Poster')
  String? poster;

  @override
  Search fromJson(Map<String, dynamic> json) {
    return _$SearchFromJson(json);
  }

  factory Search.fromJson(Map<String, dynamic> json) {
    return _$SearchFromJson(json);
  }
  @override
  Map<String, dynamic> toJson() {
    return _$SearchToJson(this);
  }
}
