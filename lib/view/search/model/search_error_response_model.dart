import 'package:json_annotation/json_annotation.dart';
import 'package:vexana/vexana.dart';

part 'search_error_response_model.g.dart';

@JsonSerializable()
class SearchErrorResponseModel extends INetworkModel<SearchErrorResponseModel> {
  SearchErrorResponseModel({
    this.response,
    this.error,
  });

  @JsonKey(name: 'Response')
  String? response;
  @JsonKey(name: 'Error')
  String? error;

  @override
  SearchErrorResponseModel fromJson(Map<String, dynamic> json) {
    return _$SearchErrorResponseModelFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$SearchErrorResponseModelToJson(this);
  }
}
