// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_error_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchErrorResponseModel _$SearchErrorResponseModelFromJson(
    Map<String, dynamic> json) {
  return SearchErrorResponseModel(
    response: json['Response'] as String?,
    error: json['Error'] as String?,
  );
}

Map<String, dynamic> _$SearchErrorResponseModelToJson(
        SearchErrorResponseModel instance) =>
    <String, dynamic>{
      'Response': instance.response,
      'Error': instance.error,
    };
