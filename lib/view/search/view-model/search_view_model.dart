import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

import '../../../core/base/model/base_view_model.dart';
import '../../../core/constant/app/api_constants.dart';
import '../../../core/constant/enum/app_theme_enum.dart';
import '../../../core/constant/enum/locale_keys_enum.dart';
import '../../../core/constant/navigation/navigation_constants.dart';
import '../../../core/extension/string_extension.dart';
import '../../../core/init/language/locale_keys.g.dart';
import '../../../core/init/network/vexana_manager.dart';
import '../../../core/init/notifier/theme_notifier.dart';
import '../../../product/constant/splash_constant.dart';
import '../../splash/view-model/splash_view_model.dart';
import '../model/search_response_model.dart';
import '../service/ISearchService.dart';
import '../service/search_service.dart';

part 'search_view_model.g.dart';

class SearchViewModel = _SearchViewModelBase with _$SearchViewModel;

abstract class _SearchViewModelBase with Store, BaseViewModel {
  late ISearchService searchService;
  late SplashViewModel splashViewModel;
  var formState = GlobalKey<FormState>();
  TextEditingController? searchController;
  ScrollController? scrollController;

  @override
  void setContext(BuildContext context) {
    this.context = context;
  }

  @override
  Future<void> init({dynamic moveData}) async {
    searchService = SearchService(VexanaManager.instance.movieManager);
    searchListResult = SearchResponseModel();
    searchController = TextEditingController();
    scrollController = ScrollController();
    searchList = <Search>[];

    splashViewModel = SplashViewModel();
  }

  //*kontrol değişkenleri
  int page = 1;
  @observable
  bool isLoading = false;
  @action
  void isLoadingChange() => isLoading = !isLoading;

  @observable
  bool isDark = SplashConstant.instance.isDarkTheme ?? false;

  //* Tema değişikliği
  @action
  Future<void> onThemeChange(BuildContext context) async {
    context.read<ThemeNotifier>().changeValue(isDark ? AppThemes.LIGHT : AppThemes.DARK);
    isDark = !isDark;
    SplashConstant.instance.isDarkTheme = isDark;
    await localeManager.setBoolValue(PreferenceKeys.IS_DARK_THEME, isDark);
  }

  //* Diğer sayfaları çağırmak için  sayfa devamı kontrolü
  @action
  bool isHasMoreLoading() {
    if (searchListResult!.response == 'True') {
      return true;
    } else {
      return false;
    }
  }

  @observable
  SearchResponseModel? searchListResult;
  @observable
  List<Search>? searchList;

  //* İstek sonrası eleman varmı kontrolü
  @action
  bool isThereAnyItem() {
    if (isLoading) {
      return false;
    } else if (searchList == null) {
      return false;
    } else {
      return true;
    }
  }

  //* Scroll hareketi ile sayfa devamındaki filmleri getirmek için kullanılır
  @action
  Future<void> moreFetchMovie() async {
    if (isHasMoreLoading() && !isLoading) {
      isLoadingChange();
      page++;
      await fetchMovie(page, single: false);
      isLoadingChange();
    }
  }

  //* Refresh indicator ile sayfa yenilendiğinde  geçmiş dataları temizleyip, yeni datanın ilk sayfasını getirir
  @action
  Future<void> refreshList() async {
    if (!isLoading) {
      isLoadingChange();
      page = 1;
      searchList = <Search>[];
      await fetchMovie(page);
      isLoadingChange();
    }
  }

  //* Sayfa bilgisi verilerek Api'den filmleri getirir
  @action
  Future<SearchResponseModel?> fetchMovie(int page, {bool single = true}) async {
    //* Form validasyonu sağlar. Arama kutucuğunda veri olup olmadığı kontrol edilir
    if (formState.currentState!.validate()) {
      isLoadingChange();

      var params = {ApiConstants.P_S: searchController!.text.trim(), ApiConstants.P_PAGE: page};
      var response = await searchService.fetchMovie(params);

      if (response != null) {
        if (response.response == 'True') {
          searchListResult = response;
          searchList!.addAll(searchListResult!.search!);
          isLoadingChange();
          return response;
        } else {
          page--;
          isLoadingChange();
          if (single) await showErrorMessage(context!, LocaleKeys.infoHeader.locale, LocaleKeys.search_movieNotFound.locale);

          return response;
        }
      } else {
        isLoadingChange();
        return response;
      }
    }
  }

  //* Film ID ile favoriye kayıtlı olup olmadığı kontrol edilir
  bool isFavoriteItem(String imdbID) {
    if (SplashConstant.instance.favoriteList != null) {
      var isFavorite = SplashConstant.instance.favoriteList!.contains(imdbID);
      return isFavorite;
    } else {
      return false;
    }
  }

  //* Favori sayfasını açmak için kullanılır
  Future<void> gotoFavoriteView() async {
    await navigation.navigateToPage(path: NavigationConstants.FAVORITE_VIEW);
  }

  //* Favoriye eklenen veya favoriden kaldırılan  filmleri telefon hafızasında günceller
  //* vvv

  Future<void> favoriteListAdd(String imdbID) async {
    await splashViewModel.localeManagerFavoriteListAdd(imdbID);
    showToast(LocaleKeys.addFavoriteList.locale);
  }

  Future<void> favoriteListRemove(String imdbID) async {
    await splashViewModel.localeManagerFavoriteListRemove(imdbID);
    showToast(LocaleKeys.removeFavoriteList.locale);
  }
}
