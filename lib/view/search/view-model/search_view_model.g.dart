// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SearchViewModel on _SearchViewModelBase, Store {
  final _$isLoadingAtom = Atom(name: '_SearchViewModelBase.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$isDarkAtom = Atom(name: '_SearchViewModelBase.isDark');

  @override
  bool get isDark {
    _$isDarkAtom.reportRead();
    return super.isDark;
  }

  @override
  set isDark(bool value) {
    _$isDarkAtom.reportWrite(value, super.isDark, () {
      super.isDark = value;
    });
  }

  final _$searchListResultAtom =
      Atom(name: '_SearchViewModelBase.searchListResult');

  @override
  SearchResponseModel? get searchListResult {
    _$searchListResultAtom.reportRead();
    return super.searchListResult;
  }

  @override
  set searchListResult(SearchResponseModel? value) {
    _$searchListResultAtom.reportWrite(value, super.searchListResult, () {
      super.searchListResult = value;
    });
  }

  final _$searchListAtom = Atom(name: '_SearchViewModelBase.searchList');

  @override
  List<Search>? get searchList {
    _$searchListAtom.reportRead();
    return super.searchList;
  }

  @override
  set searchList(List<Search>? value) {
    _$searchListAtom.reportWrite(value, super.searchList, () {
      super.searchList = value;
    });
  }

  final _$onThemeChangeAsyncAction =
      AsyncAction('_SearchViewModelBase.onThemeChange');

  @override
  Future<void> onThemeChange(BuildContext context) {
    return _$onThemeChangeAsyncAction.run(() => super.onThemeChange(context));
  }

  final _$moreFetchMovieAsyncAction =
      AsyncAction('_SearchViewModelBase.moreFetchMovie');

  @override
  Future<void> moreFetchMovie() {
    return _$moreFetchMovieAsyncAction.run(() => super.moreFetchMovie());
  }

  final _$refreshListAsyncAction =
      AsyncAction('_SearchViewModelBase.refreshList');

  @override
  Future<void> refreshList() {
    return _$refreshListAsyncAction.run(() => super.refreshList());
  }

  final _$fetchMovieAsyncAction =
      AsyncAction('_SearchViewModelBase.fetchMovie');

  @override
  Future<SearchResponseModel?> fetchMovie(int page, {bool single = true}) {
    return _$fetchMovieAsyncAction
        .run(() => super.fetchMovie(page, single: single));
  }

  final _$_SearchViewModelBaseActionController =
      ActionController(name: '_SearchViewModelBase');

  @override
  void isLoadingChange() {
    final _$actionInfo = _$_SearchViewModelBaseActionController.startAction(
        name: '_SearchViewModelBase.isLoadingChange');
    try {
      return super.isLoadingChange();
    } finally {
      _$_SearchViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool isHasMoreLoading() {
    final _$actionInfo = _$_SearchViewModelBaseActionController.startAction(
        name: '_SearchViewModelBase.isHasMoreLoading');
    try {
      return super.isHasMoreLoading();
    } finally {
      _$_SearchViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool isThereAnyItem() {
    final _$actionInfo = _$_SearchViewModelBaseActionController.startAction(
        name: '_SearchViewModelBase.isThereAnyItem');
    try {
      return super.isThereAnyItem();
    } finally {
      _$_SearchViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isLoading: ${isLoading},
isDark: ${isDark},
searchListResult: ${searchListResult},
searchList: ${searchList}
    ''';
  }
}
