import 'package:flutter/material.dart';

import '../../../core/extension/context_extension.dart';

class SimpleAlertDialog extends StatelessWidget {
  final String header;
  final String message;
  SimpleAlertDialog({Key? key, required this.header, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 10,
      backgroundColor: context.colors.error,
      titlePadding: EdgeInsets.only(left: 1, right: 1, top: 1),
      title: Column(
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            CloseButton(color: Colors.transparent),
            Text(header, style: context.textTheme.bodyText1!.copyWith(color: context.colors.onBackground)),
            CloseButton(color: context.colors.onBackground),
          ]),
          Text(message, style: context.textTheme.bodyText1!.copyWith(color: context.colors.onBackground)),
          context.emptyNormalHeightBox,
          context.emptyNormalHeightBox,
        ],
      ),
    );
  }
}
