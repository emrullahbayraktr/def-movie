import 'package:flutter/material.dart';

import '../../../core/component/button/eleveted_button.dart';
import '../../../core/extension/context_extension.dart';
import '../../../core/extension/string_extension.dart';
import '../../../core/init/language/locale_keys.g.dart';
import '../../../view/favorite/view-model/favorite_view_model.dart';

class ButtonAlertDialog extends StatelessWidget {
  final String header;
  final String message;
  final String imdbId;
  final FavoriteViewModel state;

  ButtonAlertDialog({Key? key, required this.header, required this.message, required this.state, required this.imdbId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 10,
      titlePadding: EdgeInsets.only(left: 1, right: 1, top: 1),
      title: Column(
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            CloseButton(color: Colors.transparent),
            Text(header, style: context.textTheme.bodyText1),
            CloseButton(color: context.colors.onError),
          ]),
          Text(message, style: context.textTheme.bodyText1),
          context.emptyNormalHeightBox,
          context.emptyNormalHeightBox,
        ],
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            _yesButton(context),
            context.emptyLowWithBox,
            _noButton(context),
          ],
        )
      ],
    );
  }

  QElevetedButton _noButton(BuildContext context) => QElevetedButton(onPressed: () => Navigator.pop(context), child: Text(LocaleKeys.noText.locale));

  OutlinedButton _yesButton(BuildContext context) {
    return OutlinedButton(
        onPressed: () async {
          await state.splashViewModel.localeManagerFavoriteListRemove(imdbId);
          Navigator.pop(context);
          await state.fetchSearchById();
        },
        child: Text(LocaleKeys.yesText.locale));
  }
}
