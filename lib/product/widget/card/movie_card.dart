import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';

import '../../../core/component/button/eleveted_button.dart';
import '../../../core/extension/context_extension.dart';
import '../../../core/extension/string_extension.dart';
import '../../../core/extension/widget_extension.dart';
import '../../../core/init/language/locale_keys.g.dart';
import '../../../core/init/theme/light/color_scheme_light.dart';

class MovieCard extends StatelessWidget {
  final String poster;
  final String title;
  final VoidCallback onPressed;
  final bool isFavoriteItem;

  MovieCard({Key? key, required this.onPressed, required this.poster, required this.title, required this.isFavoriteItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              movieImage(context),
              context.emptyLowHeightBox,
              movieName(context),
              context.emptyLowHeightBox,
              addFavoriteButton(context).toVisible(!isFavoriteItem), //Favori ise gizle
              removeFavoriteButton(context).toVisible(isFavoriteItem), //Favori ise göster
            ],
          )
        ],
      ),
    );
  }

  AspectRatio movieImage(BuildContext context) {
    //Verilen  en boy oranına göre widgetı sarmalar
    return AspectRatio(
      aspectRatio: 0.75,
      child: Container(
        child: ExtendedImage.network(
          poster.replaceAll('N/A', ''),
          cache: true,
        ),
      ),
    );
  }

  Widget movieName(BuildContext context) {
    return Padding(
      padding: context.paddingLowHorizontal,
      child: Text(title, maxLines: 1, style: context.textTheme.bodyText1!.copyWith(fontWeight: FontWeight.w400)),
    );
  }

  Widget addFavoriteButton(BuildContext context) {
    return Padding(
      padding: context.paddingLowHorizontal,
      child: SizedBox(
          height: context.dynamicHeight(0.035),
          child: QElevetedButton(
              onPressed: () => onPressed(),
              color: ColorSchemeLight.instance.green500,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Icon(Icons.add), Text(LocaleKeys.addFavorite.locale), SizedBox.shrink()],
              ))),
    );
  }

  Widget removeFavoriteButton(BuildContext context) {
    return Padding(
      padding: context.paddingLowHorizontal,
      child: SizedBox(
          height: context.dynamicHeight(0.035),
          child: QElevetedButton(
              onPressed: () => onPressed(),
              color: ColorSchemeLight.instance.red.withOpacity(0.7),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Icon(Icons.remove), Text(LocaleKeys.removeFavorite.locale)],
              ))),
    );
  }
}
