import '../../core/extension/string_extension.dart';

class ImagePath {
  static ImagePath? _instance;
  static ImagePath get instance => _instance ??= ImagePath._init();
  ImagePath._init();

  final dark = 'dark'.toPNG;
  final light = 'light'.toPNG;
}
