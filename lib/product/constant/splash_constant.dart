class SplashConstant {
  static SplashConstant? _instance;
  static SplashConstant get instance => _instance ??= SplashConstant._init();
  SplashConstant._init();

  bool? isDarkTheme;
  List<String>? favoriteList;
}
