import '../../core/extension/string_extension.dart';

class JsonPath {
  static JsonPath? _instance;
  static JsonPath get instance => _instance ?? JsonPath.init();

  JsonPath.init();

  final splash = 'splash'.toJson;
}
