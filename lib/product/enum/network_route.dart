enum NetworkRoute { SEARCH }

extension NetwokRoutesString on NetworkRoute {
  String get rawValue {
    switch (this) {
      case NetworkRoute.SEARCH:
        return '/search';

      default:
        return 'Not path';
    }
  }
}
