import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/constant/app/app_constants.dart';
import 'core/constant/enum/locale_keys_enum.dart';
import 'core/init/language/language_manager.dart';
import 'core/init/navigation/navigation_route.dart';
import 'core/init/navigation/navigation_service.dart';
import 'core/init/notifier/provider_list.dart';
import 'core/init/notifier/theme_notifier.dart';
import 'product/constant/splash_constant.dart';
import 'view/splash/view/splash_view.dart';

void main() async {
  await init();

  runApp(MultiProvider(
    providers: [...AppProvider.instance.dependItems], //  dependItems(),
    child: easyLocalization(),
  ));
}

// ignore: always_declare_return_types
init() async {
  WidgetsFlutterBinding.ensureInitialized();
  //*status barı siyah renklendirmek için kullanılır
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.black, statusBarBrightness: Brightness.dark));
  //*yatay dönme özelliğini kapatmak için kullanılır
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  //*env dosyasından base url bilgisini okumak için kullanılır
  await dotenv.load(fileName: '.env');

  //*Locale managerdan tema bilgisi okunuyor
  var _pref = await SharedPreferences.getInstance();
  SplashConstant.instance.isDarkTheme = _pref.getBool(PreferenceKeys.IS_DARK_THEME.toString()) ?? false;
}

EasyLocalization easyLocalization() {
  return EasyLocalization(
    supportedLocales: LanguageManager.instance!.supportedLocales,
    path: AppConstants.LANG_ASSET_PATH,
    startLocale: LanguageManager.instance!.trLocale,
    child: MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      debugShowCheckedModeBanner: false,
      title: 'Def Movie',
      theme: Provider.of<ThemeNotifier>(context).currentTheme,
      home: SplashView(),
      onGenerateRoute: NavigationRoute.instance.generateRoute,
      navigatorKey: NavigationService.instance.navigatorKey,
    );
  }
}
