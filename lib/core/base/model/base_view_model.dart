import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../../product/widget/alert-dialog/simple_alert_dialog.dart';
import '../../component/toast/toast_message_body.dart';
import '../../init/cache/locale_manager.dart';
import '../../init/navigation/navigation_service.dart';
import '../../init/network/connectivity_manager.dart';

abstract class BaseViewModel {
  BuildContext? context;
  late FToast fToast;

  LocaleManager localeManager = LocaleManager.instance;
  NavigationService navigation = NavigationService.instance;
  ConnectivityManager connectivity = ConnectivityManager.instance;

  void setContext(BuildContext context);
  void init({dynamic moveData});

  Future<void> showErrorMessage(BuildContext context, String header, String message) async {
    await showDialog(
        context: context,
        builder: (context) {
          return SimpleAlertDialog(header: header, message: message); // alertDialog(context, header, message);
        });
  }

  void showToast(String message) {
    fToast = FToast();
    fToast.init(context!);

    fToast.showToast(
        child: ToastMessageBody(text: message, color: Colors.green.shade200),
        gravity: ToastGravity.BOTTOM,
        toastDuration: Duration(milliseconds: 700));
  }
}
