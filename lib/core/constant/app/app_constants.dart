import '../enum/font_name_enum.dart';

class AppConstants {
  static const LANG_ASSET_PATH = 'asset/language';
  static final FONT_FAMILY = FontFamily.NUNITO; //'NUNITO';
  static final FONT_FAMILY_SECOND = FontFamily.NUNITO; //'ROBOTO';
  static const double elevation = 5;
}
