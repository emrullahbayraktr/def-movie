class ApiConstants {
  static const P_S = 's';
  static const P_PAGE = 'page';
  static const P_I = 'i';
  static const P_PLOT = 'plot';
}
