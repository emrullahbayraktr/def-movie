class NavigationConstants {
  static const SPLASH_VIEW = '/splash';
  static const SEARCH_VIEW = '/search';
  static const FAVORITE_VIEW = '/favorite';
  static const MOVIE_DETAIL_VIEW = '/movieDetail';
}
