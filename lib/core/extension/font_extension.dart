import '../constant/enum/font_name_enum.dart';

extension ExtFontExtension on FontFamily {
  String get rawValue {
    switch (this) {
      case FontFamily.NUNITO:
        return 'NUNITO';
      case FontFamily.ROBOTO:
        return 'ROBOTO';
      default:
        return 'NUNITO';
    }
  }
}
