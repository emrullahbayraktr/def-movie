import 'package:flutter/material.dart';

extension ExtWidgetExtension on Widget {
  Widget toVisible(bool value) => Visibility(visible: value, child: this);
}
