import '../constant/enum/http_request_enum.dart';

extension ExtNetworkExtension on HttpTypes {
  String get rawValue {
    switch (this) {
      case HttpTypes.GET:
        return 'GET';
      case HttpTypes.POST:
        return 'POST';
      case HttpTypes.PUT:
        return 'PUT';
      case HttpTypes.DEL:
        return 'DEL';
      default:
        throw 'ERROR TYPE';
    }
  }
}
