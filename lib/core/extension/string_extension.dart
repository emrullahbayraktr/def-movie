import 'package:easy_localization/easy_localization.dart';

extension ExtStringEasyLocalization on String {
  String get locale => this.tr();
}

extension ExtStringImagePathExtension on String {
  String get toSVG => 'asset/svg/$this.svg';
  String get toPNG => 'asset/image/$this.png';
  String get toJPG => 'asset/image/$this.jpg';
  String get toJPEG => 'asset/image/$this.jpeg';
  String get toGIF => 'asset/image/$this.gif';
  String get toJson => 'asset/json/$this.json';
}

extension AuthorizationExtension on String {
  Map<String, dynamic> get beraer => {'Authorization': 'Bearer ${this}'};
}
