import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

extension ExtContextExtension on BuildContext {
  MediaQueryData get mediaQuery => MediaQuery.of(this);
}

extension ExtMediaQueryExtension on BuildContext {
  double get height => mediaQuery.size.height;
  double get width => mediaQuery.size.width;

  double get lowValue => width * 0.01;
  double get normalValue => width * 0.02;
  double get mediumValue => width * 0.04;
  double get heightValue => width * 0.08;

  double dynamicHeight(double val) => height * val;
  double dynamicWidth(double val) => width * val;

  dynamic get getDeviceHeight => debugPrint('[ ===== DEVICE HEIGHT : ${height.toString()} ===== ]');
  dynamic get getDeviceWidth => debugPrint('[ ===== DEVICE WIDTH : ${width.toString()} ===== ]');
}

extension ExtThemeExtension on BuildContext {
  ThemeData get theme => Theme.of(this);
  TextTheme get textTheme => theme.textTheme;
  ColorScheme get colors => theme.colorScheme;
}

extension ExtPaddingExtension on BuildContext {
  EdgeInsets get paddingLow => EdgeInsets.all(lowValue);
  EdgeInsets get paddingNormal => EdgeInsets.all(normalValue);
  EdgeInsets get paddingMedium => EdgeInsets.all(mediumValue);
  EdgeInsets get paddingHeight => EdgeInsets.all(heightValue);
}

extension ExtPaddingHorizontalExtension on BuildContext {
  EdgeInsets get paddingLowHorizontal => EdgeInsets.symmetric(horizontal: lowValue);
  EdgeInsets get paddingNormalHorizontal => EdgeInsets.symmetric(horizontal: normalValue);
  EdgeInsets get paddingMediumHorizontal => EdgeInsets.symmetric(horizontal: mediumValue);
  EdgeInsets get paddingHeightHorizontal => EdgeInsets.symmetric(horizontal: heightValue);
}

extension ExtPaddingVerticalExtension on BuildContext {
  EdgeInsets get paddingLowVertical => EdgeInsets.symmetric(vertical: lowValue);
  EdgeInsets get paddingNormalVertical => EdgeInsets.symmetric(vertical: normalValue);
  EdgeInsets get paddingMediumVertical => EdgeInsets.symmetric(vertical: mediumValue);
  EdgeInsets get paddingHeightVertical => EdgeInsets.symmetric(vertical: heightValue);
}

extension ExtSizedBoxExtension on BuildContext {
  Widget get emptyLowWithBox => SizedBox(width: lowValue);
  Widget get emptyNormalWithBox => SizedBox(width: normalValue);
  Widget get emptyMediumWithBox => SizedBox(width: mediumValue);
  Widget get emptyHeightWithBox => SizedBox(width: heightValue);

  Widget get emptyLowHeightBox => SizedBox(height: lowValue);
  Widget get emptyNormalHeightBox => SizedBox(height: normalValue);
  Widget get emptyMediumHeightBox => SizedBox(height: mediumValue);
  Widget get emptyHeightHeightBox => SizedBox(height: heightValue);
}

extension ExtBorderRadiusExtension on BuildContext {
  Radius get radiusLow => Radius.circular(5);
  Radius get radiusNormal => Radius.circular(10);
  Radius get radiusMedium => Radius.circular(15);
  Radius get radiusHeight => Radius.circular(20);

  BorderRadius get borderRadiusLow => BorderRadius.circular(5);
  BorderRadius get borderRadiusNormal => BorderRadius.circular(10);
  BorderRadius get borderRadiusMedium => BorderRadius.circular(15);
  BorderRadius get borderRadiusHeight => BorderRadius.circular(20);
}

extension ExtPageExtension on BuildContext {
  Color get randomColor => Colors.primaries[Random().nextInt(17)];
}

extension ExtDurationExtension on BuildContext {
  Duration get durationLow => Duration(milliseconds: 500);
  Duration get duratiomNormal => Duration(seconds: 1);
  Duration get duratiomMedium => Duration(seconds: 2);
  Duration get duratiomHeight => Duration(seconds: 4);
}

extension ExtTimeFormatExtension on BuildContext {
  String timeFormatDate(DateTime date) => DateFormat.MMMMd('tr_TR').format(date).toUpperCase();
  String timeFormatDay(DateTime date) => DateFormat('dd-MM-yyyy').format(date).toString();
  String timeFormatHMS(DateTime date) => DateFormat('H:m:s').format(date).toString();
  String timeFormatHM(DateTime date) => DateFormat('H:mm').format(date).toString();
}

extension ExtBoxShadowExtension on BuildContext {
  List<BoxShadow> get shadowBlack => [BoxShadow(color: Colors.black12, offset: Offset(2, 4), blurRadius: 3)];
  List<BoxShadow> get shadowWhite => [BoxShadow(color: Colors.white60, offset: Offset(2, 4), blurRadius: 3)];
}
