import 'dart:math';

extension ExtIntegerExtension on int {
  int get randomInt => Random().nextInt(255);
  int get randomColorValue => Random().nextInt(17);
}
