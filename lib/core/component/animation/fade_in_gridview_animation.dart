import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import '../../extension/context_extension.dart';

class QFadeInAnimationGridViewConfig extends StatelessWidget {
  final int index;
  final int columnCount;
  final Widget child;
  const QFadeInAnimationGridViewConfig({Key? key, required this.index, required this.child, required this.columnCount}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimationConfiguration.staggeredGrid(
      position: index,
      columnCount: columnCount,
      duration: context.durationLow,
      child: SlideAnimation(
        verticalOffset: 50.0,
        child: FadeInAnimation(child: child),
      ),
    );
  }
}
