import 'package:flutter/material.dart';

import '../../extension/context_extension.dart';

class FormColumn extends StatelessWidget {
  final List<Widget> children;

  const FormColumn({Key? key, required this.children}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        context.emptyMediumWithBox,
        Expanded(child: Column(children: children)),
        context.emptyMediumWithBox,
      ],
    );
  }
}
