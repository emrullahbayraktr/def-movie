import 'package:flutter/material.dart';

import '../../extension/context_extension.dart';

class ToastMessageBody extends StatefulWidget {
  final String text;
  final Color color;
  ToastMessageBody({Key? key, required this.text, required this.color}) : super(key: key);

  @override
  _ErrorToastMessageState createState() => _ErrorToastMessageState();
}

class _ErrorToastMessageState extends State<ToastMessageBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: context.paddingNormal,
      decoration: BoxDecoration(borderRadius: context.borderRadiusHeight, color: widget.color),
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        runAlignment: WrapAlignment.center,
        alignment: WrapAlignment.center,
        children: [Icon(Icons.nearby_error_sharp, color: Colors.white), context.emptyMediumWithBox, Text(widget.text)],
      ),
    );
  }
}
