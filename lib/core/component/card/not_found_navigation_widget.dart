import 'package:flutter/material.dart';

import '../../init/language/locale_keys.g.dart';

class NotFoundNavigationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: Text(LocaleKeys.pageNotFound)),
    );
  }
}
