import 'package:flutter/material.dart';

import '../../constant/app/app_constants.dart';
import '../../extension/context_extension.dart';

class QVerticalCircleClipCard extends StatelessWidget {
  final double position;
  final double holeRadius;
  final Widget child;

  const QVerticalCircleClipCard({
    Key? key,
    required this.position,
    required this.holeRadius,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: AppConstants.elevation,
      color: Colors.transparent,
      child: ClipPath(
        clipper: VerticalClipper(position: position, holeRadius: holeRadius),
        child: Card(
          color: Colors.transparent,
          elevation: 1,
          margin: context.paddingLow,
          child: ClipRRect(borderRadius: context.borderRadiusNormal, child: child),
        ),
      ),
    );
  }
}

class VerticalClipper extends CustomClipper<Path> {
  VerticalClipper({this.position, this.holeRadius = 16});

  double? position;
  final double holeRadius;

  @override
  Path getClip(Size size) {
    position ??= size.width - 16;
    if (position! > size.width) throw Exception('position is greater than width.');
    final path = Path()
      ..moveTo(0, 0)
      ..lineTo(position! - holeRadius, 0.0)
      ..arcToPoint(
        Offset(position!, 0),
        clockwise: false,
        radius: Radius.circular(1),
      )
      ..lineTo(size.width, 0.0)
      ..lineTo(size.width, size.height)
      ..lineTo(position!, size.height)
      ..arcToPoint(
        Offset(position! - holeRadius, size.height),
        clockwise: false,
        radius: Radius.circular(1),
      );

    path.lineTo(0.0, size.height);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper old) => old != this;
}
