import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../extension/context_extension.dart';
import '../../init/theme/light/color_scheme_light.dart';

// ignore: must_be_immutable
class QNoBorderTextFormField extends StatelessWidget {
  TextEditingController controller = TextEditingController();

  final FocusNode? currentFocusNode, nextFocusNode;
  final int maxLines;
  final String? hintText;
  final String labelText;
  final TextStyle textStyle;
  final Widget? icon;
  final Widget? suffixIcon;
  final VoidCallback? onPressed;
  final VoidCallback? onTap;
  final VoidCallback onFieldSubmitted;
  final String? Function(String) onValidator;
  final ValueChanged<String> onSaved;
  final TextInputType? textInputType;
  final Function(String)? onChangeText;
  final List<TextInputFormatter>? inputFormatters;
  final bool readOnly;
  final bool obscureText;
  QNoBorderTextFormField({
    required this.controller,
    this.hintText,
    this.icon,
    this.suffixIcon,
    this.onPressed,
    this.onTap,
    required this.onFieldSubmitted,
    required this.onValidator,
    required this.onSaved,
    required this.labelText,
    this.textInputType,
    this.onChangeText,
    this.currentFocusNode,
    this.nextFocusNode,
    required this.textStyle,
    this.maxLines = 1,
    this.inputFormatters,
    this.readOnly = false,
    this.obscureText = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border.all(color: ColorSchemeLight.instance.gray.withOpacity(0.15))),
      padding: context.paddingLow,
      child: TextFormField(
        textAlign: TextAlign.center,
        readOnly: readOnly,
        controller: controller,
        onTap: onTap,
        validator: (value) => value!.isEmpty ? onValidator(value) : null,
        obscureText: obscureText,
        keyboardType: textInputType,
        inputFormatters: inputFormatters,
        maxLines: maxLines,
        style: textStyle,
        textInputAction: TextInputAction.next,
        focusNode: currentFocusNode,
        onFieldSubmitted: (value) => onFieldSubmitted,
        onSaved: (newValue) => onSaved(newValue!),
        decoration: InputDecoration(
          // border: InputBorder.none,
          hintText: hintText,
          hintStyle: context.textTheme.bodyText1,
          // hintStyle: context.textTheme.bodyText1!.copyWith(color: Colors.grey[400]),
          labelText: labelText == '' ? null : labelText,
          icon: icon == null ? SizedBox.shrink() : icon!,
          suffixIcon: suffixIcon == null ? SizedBox.shrink() : suffixIcon!,
          // suffixIcon: suffixIcon == null ? SizedBox.shrink() : IconButton(padding: EdgeInsets.all(1), icon: suffixIcon!, onPressed: () => onPressed),
        ),
      ),
    );
  }
}
