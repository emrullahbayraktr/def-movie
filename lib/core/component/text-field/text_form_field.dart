import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../extension/context_extension.dart';
import '../../init/theme/light/color_scheme_light.dart';

// ignore: must_be_immutable
class QBorderTextFormField extends StatelessWidget {
  TextEditingController controller = TextEditingController();

  final FocusNode? currentFocusNode, nextFocusNode;
  final int maxLines;
  final String? hintText;
  final String labelText;
  final TextStyle textStyle;
  final Widget icon;
  final VoidCallback? onPressed;
  final VoidCallback onFieldSubmitted;
  final String Function(String) onValidator;
  final ValueChanged<String> onSaved;
  final TextInputType? textInputType;
  final Function(String)? onChangeText;
  final List<TextInputFormatter>? inputFormatters;
  final bool readOnly;
  QBorderTextFormField(
      {required this.controller,
      this.hintText,
      required this.icon,
      this.onPressed,
      required this.onFieldSubmitted,
      required this.onValidator,
      required this.onSaved,
      required this.labelText,
      this.textInputType,
      this.onChangeText,
      this.currentFocusNode,
      this.nextFocusNode,
      required this.textStyle,
      this.maxLines = 1,
      this.inputFormatters,
      this.readOnly = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border.all(color: ColorSchemeLight.instance.gray.withOpacity(0.5))),
      child: TextFormField(
        readOnly: readOnly,
        keyboardType: textInputType,
        inputFormatters: inputFormatters,
        maxLines: maxLines,
        style: textStyle,
        controller: controller,
        textInputAction: TextInputAction.next,
        focusNode: currentFocusNode,
        onFieldSubmitted: (value) => onFieldSubmitted,
        validator: (value) => value!.isEmpty ? onValidator(value) : null,
        onSaved: (newValue) => onSaved(newValue!),
        decoration: InputDecoration(
          isDense: true,
          contentPadding: EdgeInsets.symmetric(horizontal: context.dynamicWidth(0.027), vertical: context.dynamicHeight(0.013)),
          // border: OutlineInputBorder(borderRadius: context.borderRadiusLow),
          // errorBorder: InputBorder.none,
          // enabledBorder: InputBorder.none,
          // focusedBorder: InputBorder.none,
          // focusedErrorBorder: InputBorder.none,
          hintText: hintText,
          labelText: labelText == '' ? null : labelText,
          suffixIcon: IconButton(padding: EdgeInsets.all(1), icon: icon, onPressed: () => onPressed),
        ),
      ),
    );
  }
}
