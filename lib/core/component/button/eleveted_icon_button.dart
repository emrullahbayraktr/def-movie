import 'package:flutter/material.dart';

import '../../extension/context_extension.dart';
import 'eleveted_button.dart';

class QElevetedIconButton extends StatelessWidget {
  final IconData icon;
  final String label;
  final VoidCallback onPressed;
  const QElevetedIconButton({Key? key, required this.icon, required this.label, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: context.paddingLow,
      child: QElevetedButton(
          onPressed: onPressed,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(icon),
              context.emptyLowWithBox,
              Text(label),
            ],
          )),
    );
  }
}
