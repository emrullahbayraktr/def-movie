import 'package:flutter/material.dart';

import '../../constant/app/app_constants.dart';
import '../../extension/context_extension.dart';

class QElevetedButton extends StatelessWidget {
  final Widget child;
  final VoidCallback onPressed;
  final Color? color;

  QElevetedButton({Key? key, required this.child, required this.onPressed, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        elevation: AppConstants.elevation,
        primary: color ?? context.colors.error.withOpacity(.6),
        shape: RoundedRectangleBorder(borderRadius: context.borderRadiusLow),
      ),
      onPressed: onPressed,
      child: Center(child: child),
    );
  }
}
