import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../product/constant/json_path.dart';

class LoadingForm extends StatelessWidget {
  const LoadingForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Spacer(flex: 4),
          Expanded(flex: 4, child: LottieBuilder.asset(JsonPath.instance.splash)),
          Spacer(flex: 4),
        ],
      ),
    );
  }
}
