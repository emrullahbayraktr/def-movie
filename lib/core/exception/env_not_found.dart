class EnvNotFound implements Exception {
  final String? value;

  EnvNotFound(this.value);

  @override
  String toString() {
    return 'Thiis $value does not  found in env file';
  }
}
