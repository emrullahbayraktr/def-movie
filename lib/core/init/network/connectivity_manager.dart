import 'package:connectivity/connectivity.dart';

class ConnectivityManager {
  static ConnectivityManager? _instance;
  static ConnectivityManager get instance => _instance ??= ConnectivityManager._init();
  ConnectivityManager._init();

  Future<bool> connectivityStatus() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
      // throw NetworkExceptions.noInternetConnection();
    }
  }
}
