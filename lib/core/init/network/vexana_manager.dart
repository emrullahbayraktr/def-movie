import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:vexana/vexana.dart';

import '../../../view/search/model/search_error_response_model.dart';
import '../../exception/env_not_found.dart';

class VexanaManager {
  static VexanaManager? _instance;
  static VexanaManager get instance => _instance ??= VexanaManager._init();

  VexanaManager._init() {
    var url = dotenv.env[_baseMovieUrl];

    //debug modda log içeriği görünsün isterinse true ya çekilmeli isEnableLogger: kDebugMode ? true : false,
    if (url != null) {
      movieManager =
          NetworkManager(isEnableLogger: kDebugMode ? false : false, options: BaseOptions(baseUrl: url), errorModel: SearchErrorResponseModel());
    } else {
      throw EnvNotFound(url);
    }
  }

  final _baseMovieUrl = 'BASE_MOVIE_URL';
  late INetworkManager movieManager;
}
