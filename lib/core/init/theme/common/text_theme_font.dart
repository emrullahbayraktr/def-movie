import 'package:flutter/cupertino.dart';

class TextThemeFont {
  static TextThemeFont? _instance;
  static TextThemeFont get instance => _instance ??= TextThemeFont._init();
  TextThemeFont._init();
  final TextStyle headline1 = TextStyle(fontSize: 96, fontWeight: FontWeight.w300, letterSpacing: -1.5);
  final TextStyle headline2 = TextStyle(fontSize: 60, fontWeight: FontWeight.w300, letterSpacing: -0.5);
  final TextStyle headline3 = TextStyle(fontSize: 48, fontWeight: FontWeight.w400);
  final TextStyle headline4 = TextStyle(fontSize: 34, fontWeight: FontWeight.w400, letterSpacing: 0.25);
  final TextStyle headline5 = TextStyle(fontSize: 24, fontWeight: FontWeight.w400);
  final TextStyle headline6 = TextStyle(fontSize: 20, fontWeight: FontWeight.w500, letterSpacing: 0.15);
  final TextStyle subtitle1 = TextStyle(fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.15);
  final TextStyle subtitle2 = TextStyle(fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 0.1);
  final TextStyle bodyText1 = TextStyle(fontSize: 13, fontWeight: FontWeight.w400, letterSpacing: 0.5);
  final TextStyle bodyText2 = TextStyle(fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.25);
  final TextStyle button = TextStyle(fontSize: 12, fontWeight: FontWeight.w500, letterSpacing: 1.25);
  final TextStyle caption = TextStyle(fontSize: 11, fontWeight: FontWeight.w400, letterSpacing: 0.4);
  final TextStyle overline = TextStyle(fontSize: 10, fontWeight: FontWeight.w400, letterSpacing: 1.5);
}
