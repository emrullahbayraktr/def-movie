import '../common/padding_insets.dart';
import '../common/text_theme_font.dart';
import 'color_scheme_light.dart';

abstract class ILightTheme {
  TextThemeFont textThemeFont = TextThemeFont.instance;
  ColorSchemeLight colorSchemeLight = ColorSchemeLight.instance;
  PaddingInsets insets = PaddingInsets();
}
