import 'package:flutter/material.dart';

class ColorSchemeLight {
  static ColorSchemeLight? _instance;
  static ColorSchemeLight get instance => _instance ??= ColorSchemeLight._init();
  ColorSchemeLight._init();

  final Color brown = Color(0xffa87e6f);
  final Color red = Color(0xffc10e0e);
  final Color white = Color(0xffffffff);
  final Color gray = Color(0xffa5a6ae);
  final Color lightGray = Color(0xfff7f7f7);
  final Color darkGray = Color(0xff676870);
  final Color black = Color(0xff020306);

  final Color azure = Color(0xff27928d);
  final Color primary = Color(0xff6D9773);
  final Color backBlueDark = Color(0xFF1C2F5D);
  final Color backBlue = Color(0xFF1B417F);
  final Color green500 = Colors.green.shade500;
  final Color blue50 = Color(0xFFE0EBF7);
  final Color blue100 = Color(0xFFB3CEEA);
  final Color blue200 = Color(0xFF80ADDC);
  final Color blue300 = Color(0xFF4D8CCE);
  final Color blue400 = Color(0xFF2674C4);

  final Color pink = Color(0xFFE8D7DE);
  final Color blue = Color(0xFFC7E1FA);
  final Color green = Color(0xFFABDCC2);
}
