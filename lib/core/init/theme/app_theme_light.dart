import 'package:flutter/material.dart';

import '../../constant/app/app_constants.dart';
import '../../extension/font_extension.dart';
import 'app_theme.dart';
import 'light/ILightTheme.dart';

class AppThemeLight extends AppTheme with ILightTheme {
  static AppThemeLight? _instance;

  static AppThemeLight get instance {
    _instance ??= AppThemeLight._init();

    return _instance!;
  }

  AppThemeLight._init();

  // ThemeData get theme => ThemeData.light();

  ThemeData get theme => ThemeData(
      fontFamily: AppConstants.FONT_FAMILY.rawValue,
      colorScheme: _appColorScheme,
      textTheme: _appTextTheme,
      appBarTheme: _appBarTheme,
      inputDecorationTheme: InputDecorationTheme(
        enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: _appColorScheme.primary)),
        border: UnderlineInputBorder(borderSide: BorderSide(color: _appColorScheme.primary)),
        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: _appColorScheme.primary)),
      ));

  AppBarTheme get _appBarTheme => ThemeData.light().appBarTheme.copyWith(
      brightness: Brightness.light,
      backgroundColor: _appColorScheme.primaryVariant,
      iconTheme: IconThemeData(color: Colors.white),
      titleTextStyle: _appTextTheme.bodyText1!.copyWith(fontWeight: FontWeight.bold),
      centerTitle: true);

  TextTheme get _appTextTheme => ThemeData.light().textTheme.copyWith(
        headline1: textThemeFont.headline1,
        headline2: textThemeFont.headline2,
        headline3: textThemeFont.headline3,
        headline4: textThemeFont.headline4,
        headline5: textThemeFont.headline5,
        headline6: textThemeFont.headline6,
        subtitle1: textThemeFont.subtitle1,
        subtitle2: textThemeFont.subtitle2,
        bodyText1: textThemeFont.bodyText1,
        bodyText2: textThemeFont.bodyText2,
        button: textThemeFont.button,
        caption: textThemeFont.caption,
        overline: textThemeFont.overline,
      );

  ColorScheme get _appColorScheme => ColorScheme(
        primary: Color(0xFF2674C4), //Color(0xFF005BB9),
        primaryVariant: Color(0xFF1C2F5D), //Color(0xFF1B417F);
        secondary: Color(0xFF2674C4),
        secondaryVariant: Color(0xff303f9f),
        surface: Color(0xffffffff),
        background: Color(0xFFE0EBF7),
        error: Color(0xffd32f2f),
        onPrimary: Color(0xffffffff),
        onSecondary: Color(0xffffffff),
        onSurface: Color(0xff000000),
        onBackground: Color(0xffffffff),
        onError: Color(0xffffffff),
        brightness: Brightness.light,
      );
}

// const MaterialColor cankiriozlem = MaterialColor(_cankiriozlemPrimaryValue, <int, Color>{
//   50: Color(0xFFE0EBF7),
//   100: Color(0xFFB3CEEA),
//   200: Color(0xFF80ADDC),
//   300: Color(0xFF4D8CCE),
//   400: Color(0xFF2674C4),
//   500: Color(_cankiriozlemPrimaryValue),
//   600: Color(0xFF0053B2),
//   700: Color(0xFF0049AA),
//   800: Color(0xFF0040A2),
//   900: Color(0xFF002F93),
// });
// const int _cankiriozlemPrimaryValue = 0xFF005BB9;

// const MaterialColor cankiriozlemAccent = MaterialColor(_cankiriozlemAccentValue, <int, Color>{
//   100: Color(0xFFBECEFF),
//   200: Color(_cankiriozlemAccentValue),
//   400: Color(0xFF5880FF),
//   700: Color(0xFF3F6DFF),
// });
// const int _cankiriozlemAccentValue = 0xFF8BA7FF;
