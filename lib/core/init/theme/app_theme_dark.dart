import 'package:flutter/material.dart';

import '../../constant/app/app_constants.dart';
import '../../extension/font_extension.dart';
import 'app_theme.dart';
import 'dark/IDarkTheme.dart';

class AppThemeDark extends AppTheme with IDarkTheme {
  static AppThemeDark? _instance;
  static AppThemeDark get instance => _instance ??= AppThemeDark._init();
  AppThemeDark._init();

  ThemeData get theme => ThemeData(
        fontFamily: AppConstants.FONT_FAMILY.rawValue,
        colorScheme: _appColorScheme,
        textTheme: _appTextTheme,
        appBarTheme: _appBarTheme,
      );

  AppBarTheme get _appBarTheme => ThemeData.light().appBarTheme.copyWith(
      brightness: Brightness.dark,
      backgroundColor: _appColorScheme.primaryVariant,
      iconTheme: IconThemeData(color: Colors.white),
      titleTextStyle: _appTextTheme.bodyText1!.copyWith(fontWeight: FontWeight.bold),
      centerTitle: true);

  TextTheme get _appTextTheme => ThemeData.dark().textTheme.copyWith(
        headline1: textThemeFont.headline1,
        headline2: textThemeFont.headline2,
        headline3: textThemeFont.headline3,
        headline4: textThemeFont.headline4,
        headline5: textThemeFont.headline5,
        headline6: textThemeFont.headline6,
        subtitle1: textThemeFont.subtitle1,
        subtitle2: textThemeFont.subtitle2,
        bodyText1: textThemeFont.bodyText1,
        bodyText2: textThemeFont.bodyText2,
        button: textThemeFont.button,
        caption: textThemeFont.caption,
        overline: textThemeFont.overline,
      );

  ColorScheme get _appColorScheme => ColorScheme(
        primary: Color(0xFF2674C4), //Color(0xFF005BB9),
        primaryVariant: Color(0xFF1C2F5D), //Color(0xFF1B417F);
        secondary: Color(0xFF2674C4),
        secondaryVariant: Color(0xff303f9f),
        surface: Color(0xff424242),
        background: Color(0xff616161),
        error: Color(0xffd32f2f),
        onPrimary: Color(0xffffffff),
        onSecondary: Color(0xff000000),
        onSurface: Color(0xffffffff),
        onBackground: Color(0xffffffff),
        onError: Color(0xff000000),
        brightness: Brightness.dark,
      );
  // ColorScheme get _appColorScheme => ColorScheme(
  //       primary: Color(0xFF005BB9),
  //       primaryVariant: Color(0xFF0040A2),
  //       secondary: Color(0xff3f51b5),
  //       secondaryVariant: Color(0xff303f9f),
  //       surface: Color(0xffffffff),
  //       background: Color(0xFFE0EBF7),
  //       error: Color(0xffd32f2f),
  //       onPrimary: Color(0xffffffff),
  //       onSecondary: Color(0xffffffff),
  //       onSurface: Color(0xff000000),
  //       onBackground: Color(0xffffffff),
  //       onError: Color(0xffffffff),
  //       brightness: Brightness.light,
  //     );
}
