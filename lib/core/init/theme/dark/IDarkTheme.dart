import '../common/padding_insets.dart';
import '../common/text_theme_font.dart';
import 'color_scheme_dark.dart';

abstract class IDarkTheme {
  TextThemeFont textThemeFont = TextThemeFont.instance;
  ColorSchemeDark colorSchemeDark = ColorSchemeDark.instance;
  PaddingInsets insets = PaddingInsets();
}
