import 'package:flutter/cupertino.dart';

import 'INavigationService.dart';

class NavigationService implements INavigationService {
  static final NavigationService _instance = NavigationService._init();
  static NavigationService get instance => _instance;
  NavigationService._init();

  GlobalKey<NavigatorState> navigatorKey = GlobalKey();
  final removeAllOldRoutes = (route) => false;
  @override
  Future<dynamic> navigateToPage({String? path, Object? data}) async {
    var response = await navigatorKey.currentState!.pushNamed(path!, arguments: data);
    return response;
  }

  @override
  Future<void> navigateToPageClear({String? path, Object? data}) async {
    await navigatorKey.currentState!.pushNamedAndRemoveUntil(path!, removeAllOldRoutes, arguments: data);
  }

  Future<void> navigateToPop({String? ettn}) async {
    navigatorKey.currentState!.pop(ettn);
  }
}
