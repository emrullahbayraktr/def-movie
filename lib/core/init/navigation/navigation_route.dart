import 'package:flutter/material.dart';

import '../../../view/favorite/view/favorite_view.dart';
import '../../../view/search/view/search_view.dart';
import '../../component/card/not_found_navigation_widget.dart';
import '../../constant/navigation/navigation_constants.dart';

class NavigationRoute {
  static final NavigationRoute _instance = NavigationRoute._init();
  static NavigationRoute get instance => _instance;
  NavigationRoute._init();

  Route generateRoute(RouteSettings args) {
    switch (args.name) {
      case NavigationConstants.SEARCH_VIEW:
        return normalPageRoute(SearchView());
      case NavigationConstants.FAVORITE_VIEW:
        return normalPageRoute(FavoriteView());
      default:
        return MaterialPageRoute(builder: (context) => NotFoundNavigationWidget());
    }
  }

  MaterialPageRoute normalPageRoute(Widget widget) => MaterialPageRoute(builder: (context) => widget);
}
