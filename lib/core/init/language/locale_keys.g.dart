// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const appName = 'appName';
  static const noConnection = 'noConnection';
  static const pageNotFound = 'pageNotFound';
  static const errorHeader = 'errorHeader';
  static const warningHeader = 'warningHeader';
  static const infoHeader = 'infoHeader';
  static const warningMessage = 'warningMessage';
  static const addFavoriteList = 'addFavoriteList';
  static const removeFavoriteList = 'removeFavoriteList';
  static const addFavorite = 'addFavorite';
  static const removeFavorite = 'removeFavorite';
  static const yesText = 'yesText';
  static const noText = 'noText';
  static const search_searchValidator = 'search.searchValidator';
  static const search_searchLabel = 'search.searchLabel';
  static const search_infoMessage = 'search.infoMessage';
  static const search_movieNotFound = 'search.movieNotFound';
  static const search = 'search';
  static const favorite_appBarName = 'favorite.appBarName';
  static const favorite_notFoundMessage = 'favorite.notFoundMessage';
  static const favorite = 'favorite';

}
