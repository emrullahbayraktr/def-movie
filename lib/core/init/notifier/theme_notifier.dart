import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../constant/enum/app_theme_enum.dart';
import '../theme/app_theme_dark.dart';
import '../theme/app_theme_light.dart';

class ThemeNotifier extends ChangeNotifier {
  ThemeData _currentTheme = AppThemeLight.instance.theme;
  ThemeData get currentTheme => _currentTheme;

  ThemeData changeValue(AppThemes theme) {
    if (theme == AppThemes.LIGHT) {
      _currentTheme = AppThemeLight.instance.theme; // ThemeData.light();
    } else {
      _currentTheme = AppThemeDark.instance.theme;
    }
    notifyListeners();

    return _currentTheme;
  }
}
