import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import '../navigation/navigation_service.dart';
import 'theme_notifier.dart';

class AppProvider {
  static AppProvider? _instance;

  static AppProvider get instance {
    _instance ??= AppProvider._init();

    return _instance!;
  }

  AppProvider._init();

  List<SingleChildWidget> dependItems = [
    ChangeNotifierProvider(create: (context) => ThemeNotifier()),
    Provider.value(value: NavigationService.instance),
  ];
}
