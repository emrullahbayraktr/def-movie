import 'package:shared_preferences/shared_preferences.dart';

import '../../constant/enum/locale_keys_enum.dart';

class LocaleManager {
  //Singleton variable
  static final LocaleManager _instance = LocaleManager._init();
  SharedPreferences? _preferences;

  static LocaleManager get instance => _instance;

  LocaleManager._init() {
    SharedPreferences.getInstance().then((value) => _preferences = value);
  }

  // ignore: always_declare_return_types
  static get preferencesInit async {
    instance._preferences ??= await SharedPreferences.getInstance();
  }

  Future<void> setStringValue(PreferenceKeys key, String value) async {
    await _preferences!.setString(key.toString(), value);
  }

  Future<void> setStringList(PreferenceKeys key, List<String> value) async {
    await _preferences!.setStringList(key.toString(), value);
  }

  Future<void> setBoolValue(PreferenceKeys key, bool value) async {
    await _preferences!.setBool(key.toString(), value);
  }

  String? getStringValue(PreferenceKeys key) => _preferences!.getString(key.toString());

  List<String>? getStringList(PreferenceKeys key) => _preferences!.getStringList(key.toString());

  bool? getBoolValue(PreferenceKeys key) => _preferences!.getBool(key.toString()) ?? false;
}




// import 'package:shared_preferences/shared_preferences.dart';

// import '../../constant/enum/locale_keys_enum.dart';

// class LocaleManager {
//   static final LocaleManager _instance = LocaleManager._init();

//   SharedPreferences? _preferences;
//   static LocaleManager get instance => _instance;

//   LocaleManager._init() {
//     SharedPreferences.getInstance().then((value) {
//       _preferences = value;
//     });
//   }
//   static Future prefrencesInit() async {
//     instance._preferences ??= await SharedPreferences.getInstance();
//   }

//   Future<void> clearAll() async {
//     await _preferences!.clear();
//   }

//   Future<void> clearAllSaveFirst() async {
//     if (_preferences != null) {
//       await _preferences!.clear();
//       await setBoolValue(PreferenceKeys.IS_FIRST_APP, true);
//     }
//   }

//   Future<void> setStringValue(PreferenceKeys key, String value) async {
//     var pref = await SharedPreferences.getInstance();

//     await pref.setString(key.toString(), value);
//   }

//   Future<bool> setBoolValue(PreferenceKeys key, bool value) async {
//     var pref = await SharedPreferences.getInstance();

//     return pref.setBool(key.toString(), value);
//   }

//   Future<String> getStringValue(PreferenceKeys key) async {
//     var pref = await SharedPreferences.getInstance();

//     return pref.getString(key.toString()) ?? '';
//   }

//   Future<bool> getBoolValue(PreferenceKeys key) async {
//     var pref = await SharedPreferences.getInstance();

//     return pref.getBool(key.toString()) ?? false;
//   }
// }
