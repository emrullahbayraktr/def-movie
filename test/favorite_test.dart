import 'package:defmovie/core/constant/app/api_constants.dart';
import 'package:defmovie/view/favorite/model/search_by_id_response_model.dart';
import 'package:defmovie/view/favorite/service/ISearchByIdService.dart';
import 'package:defmovie/view/favorite/service/search_by_id_service.dart';
import 'package:flutter_test/flutter_test.dart';

import 'network/vexana_mock_manager.dart';

void main() {
  late ISearchByIdService searchByIdService;

  setUp(() {
    searchByIdService = SearchByIdService(VexanaMockManager.instance.movieManager);
  });
  test('Get Favorite Movie', () async {
    var params = {ApiConstants.P_I: 'tt9072706', ApiConstants.P_PLOT: 'short'};
    final response = await searchByIdService.fetchSearchById(params);

    expect(response is SearchByIdResponseModel, true);
  });
}
