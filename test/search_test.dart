import 'package:defmovie/core/constant/app/api_constants.dart';
import 'package:defmovie/view/search/model/search_response_model.dart';
import 'package:defmovie/view/search/service/ISearchService.dart';
import 'package:defmovie/view/search/service/search_service.dart';
import 'package:flutter_test/flutter_test.dart';

import 'network/vexana_mock_manager.dart';

void main() {
  late ISearchService searchService;
  setUp(() {
    searchService = SearchService(VexanaMockManager.instance.movieManager);
  });

  test('Get Movie List', () async {
    var params = {ApiConstants.P_S: 'blade', ApiConstants.P_PAGE: 1};
    final response = await searchService.fetchMovie(params);

    expect(response is SearchResponseModel, true);
  });

  test('Get Movie List Is Not Empty', () async {
    var params = {ApiConstants.P_S: 'blade', ApiConstants.P_PAGE: 1};
    final response = await searchService.fetchMovie(params);

    expect(response!.search!.isNotEmpty, true);
  });
}
