import 'package:flutter/foundation.dart';
import 'package:vexana/vexana.dart';

class VexanaMockManager {
  static VexanaMockManager? _instance;
  static VexanaMockManager get instance => _instance ??= VexanaMockManager._init();

  VexanaMockManager._init() {
    var _baseMovieUrl = 'http://www.omdbapi.com/?apikey=ae383730&';

    //debug modda log içeriği görünsün isterinse true ya çekilmeli isEnableLogger: kDebugMode ? true : false,

    movieManager = NetworkManager(isEnableLogger: kDebugMode ? false : false, options: BaseOptions(baseUrl: _baseMovieUrl));
  }

  late INetworkManager movieManager;
}
